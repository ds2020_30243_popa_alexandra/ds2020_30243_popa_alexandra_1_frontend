import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Col, Container, Jumbotron, Modal, ModalBody, ModalHeader} from 'reactstrap';
import LoginForm from "./login-form";
import * as API_USERS from "../patient/api/patient-api";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class LoginContainer extends React.Component {
    constructor(props) {
        super(props);
        this.toggleFormLogin = this.toggleFormLogin.bind(this);

        this.reload = this.reload.bind(this);
        this.state = {
            selectedLogin: false,
            isLoaded: false,
            errorStatus: 0,
            error: null,
        };
        this.props.toggleLoginOn(false);
    }

    toggleFormLogin() {
        this.setState({selectedLogin: !this.state.selectedLogin});

    }
    reload() {
        this.setState({
            isLoaded: false
        });
    }
    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    componentDidMount() {
        this.toggleFormLogin();
    }
    toggleLoginOn=(rez)=>{
        this.props.toggleLoginOn(rez);
    }



    render() {

        return (

            <div>

                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>
                    </Container>
                </Jumbotron>
                <Button color="primary" onClick={this.toggleFormLogin}>Login </Button>
                <Modal isOpen={this.state.selectedLogin} toggle={this.toggleFormLogin}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormLogin}> Login : </ModalHeader>
                    <ModalBody>
                        <LoginForm   toggleLoginOn={this.toggleLoginOn} reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    };
}

export default LoginContainer;
