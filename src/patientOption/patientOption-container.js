import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Card, CardHeader, Col, Container, Jumbotron, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';




import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import * as API_USERS from "../patient/api/patient-api"
import MedicationListTable from "../patient/components/medicationList-table";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class PatientOptionContainer extends React.Component {
    constructor(props) {
        super(props);
        this.toggleFormAdd = this.toggleFormAdd.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selectedRow:null,
            collapseForm: false,
            tableData: [],
            caregiver:[],
            isLoaded: false,
            isLoadedTable: false,
            errorStatus: 0,
            error: null,
            selectedLogin: false,
            medicationPlan:[],
            tableView:[]
        };


    }
    logOut=()=>{
        sessionStorage.removeItem('userId');
        window.location.href = '/';
    }

    componentDidMount() {
        this.fetchPatients();
        //this.fetchCaregiver();
        this.fetchMedicationPlan();
        this.props.toggleLoginOn(true);

    }

    fetchPatients() {

        return API_USERS.getPatientById(sessionStorage.getItem("userId"),(result, status, err) => {

            if (result !== null && status === 200) {
                console.log(result)
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });







    }
    fetchMedicationPlan(){
        return API_USERS.getMedicationPlanByPatientId(sessionStorage.getItem("userId"),(result, status, err) => {

            if (result !== null && status === 200) {

                let tempTableView=this.state.tableView
                result.items.forEach(item => {
                    let elemTable={
                        medicationName:item.medication.name,
                        dosage:item.medication.dosage,
                        intakeIntervals:item.intakeIntervals
                    }
                    tempTableView.push(elemTable)});
                this.setState({
                    medicationPlan:result,
                   tableView:tempTableView,
                    isLoadedTable: true
                });
                console.log(this.state.medicationPlan)
            }
        });


    }

    toggleFormAdd() {
        this.setState({selectedAdd: !this.state.selectedAdd});

    }

    selectRow=(rowIndex) =>{
        this.setState({
            selectedRow: rowIndex
        });
        sessionStorage.setItem("selectedRow", this.state.selectedRow);
    }

    reload() {
        this.setState({
            isLoaded: false,
            isLoadedTable:false
        });

        this.fetchPatients();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Personal Details </strong>
                </CardHeader>
                <Card >
                    <strong>Name : {this.state.tableData.name}</strong>
                    <strong>Birthdate : {this.state.tableData.birthDate}</strong>
                    <strong>Gender : {this.state.tableData.gender}</strong>
                    <strong>Address : {this.state.tableData.address} </strong>
                    <strong>Medical record : {this.state.tableData.medicalRecord}</strong>
                    <strong>Caregiver : {this.state.tableData.caregiver}</strong>
                </Card>
                <CardHeader>
                    <strong> Medication plan </strong>
                </CardHeader>
                <Card >
                    <strong>Period of the treatment: {this.state.medicationPlan.periodOfTheTreatment}</strong>

                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoadedTable && <MedicationListTable /*key = {this.state.tableView}*/ tableData = {this.state.tableView} />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Card >
                    <br/>
                    <Row >
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.logOut}> LogOut </Button>
                        </Col>
                    </Row>
                </Card >


            </div>
        )

    }
}

export default PatientOptionContainer;
