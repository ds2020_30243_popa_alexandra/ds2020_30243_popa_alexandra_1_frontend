import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Card, CardHeader, Col, Container, Jumbotron, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';




import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import * as API_USERS from "../patient/api/patient-api"
import * as API_CAREGIVER from "../caregiver/api/caregiver-api"
import PatientTable from "../patient/components/patient-table";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class CaregiverOptionContainer extends React.Component {
    constructor(props) {
        super(props);
        this.toggleFormAdd = this.toggleFormAdd.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selectedRow:null,
            collapseForm: false,
            tableData: [],
            caregiver:[],
            isLoaded: false,
            isLoadedTable:false,
            errorStatus: 0,
            error: null,
            selectedLogin: false,
            medicationPlan:[],
            tableView:[]
        };

    }


    componentDidMount() {

        this.fetchPatients();
        this.fetchCaregiver();
        this.props.toggleLoginOn(true);

    }

    fetchCaregiver() {

        return API_CAREGIVER.getCaregiverById(sessionStorage.getItem("userId"),(result, status, err) => {

            if (result !== null && status === 200) {

                this.setState({
                    caregiver: result,
                    isLoaded: true
                });
                console.log(this.state.caregiver)
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });


    }
    fetchPatients() {

        return API_USERS.getPatientsByCaregiverId(sessionStorage.getItem("userId"),(result, status, err) => {

            if (result !== null && status === 200) {

                this.setState({
                    tableData: result,
                    isLoadedTable: true
                });
                console.log(this.state.tableData)
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });


    }
    logOut=()=>{
        sessionStorage.removeItem('userId');
        window.location.href = '/';
    }

    toggleFormAdd() {
        this.setState({selectedAdd: !this.state.selectedAdd});

    }

    selectRow=(rowIndex) =>{
        this.setState({
            selectedRow: rowIndex
        });

        sessionStorage.setItem("selectedRow", this.state.selectedRow);
    }

    reload() {
        this.setState({
            isLoaded: false,
            isLoadedTable: false,

        });

        this.fetchCaregiver();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Personal Details </strong>
                </CardHeader>
                <Card >
                    <strong>Name : {this.state.caregiver.name}</strong>
                    <strong>Birthdate : {this.state.caregiver.birthDate}</strong>
                    <strong>Gender : {this.state.caregiver.gender}</strong>
                    <strong>Address : {this.state.caregiver.address} </strong>

                </Card>
                <CardHeader>
                    <strong> Patients </strong>
                </CardHeader>
                <Card >
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoadedTable && <PatientTable  tableData = {this.state.tableData} />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Card >
                    <br/>
                    <Row >
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.logOut}> LogOut </Button>
                        </Col>
                    </Row>
                </Card >

            </div>
        )

    }
}

export default CaregiverOptionContainer;
