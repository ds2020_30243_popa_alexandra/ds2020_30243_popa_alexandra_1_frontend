const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};
const requiredValidator = value => {
    return value.trim() !== '';
};

const dateValidator = value => {
    const re = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    return re.test(value);
};
const validate = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;
            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;
            case 'dateValidator': isValid = isValid && dateValidator(value);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validate;

