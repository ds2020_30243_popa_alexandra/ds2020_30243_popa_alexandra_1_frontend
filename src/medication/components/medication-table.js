import React from "react";
import Table from "../../commons/tables/table";
//import Checkbox from '@material-ui/core/Checkbox';

const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects',
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },

];

const filters = [
    {
        accessor: 'name',
    }
];

class MedicationTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }
    handleSelect = function(state) {
        console.log(state);
        return state.isOpen;
    }

    onMedicationRowClick=(rowInfo)=>{
        this.props.onRowClick(rowInfo)
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
                onRowClick={this.onMedicationRowClick}

            />
        )
    }
}

export default MedicationTable;