import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicationForm from "./components/medication-form";
import MedicationUpdateForm from "./components/medication-update-form";

import * as API_USERS from "./api/medication-api"
import MedicationTable from "./components/medication-table";



class MedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormAdd = this.toggleFormAdd.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedAdd: false,
            selectedDelete: false,
            selectedUpdate: false,
            selectedRow:null,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteMedication(medicationId) {
        return API_USERS.deleteMedication(medicationId, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted medication with id: " + result);
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    toggleFormAdd() {
        this.setState({selectedAdd: !this.state.selectedAdd});

    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
        console.log(this.state.selectedRow.id);
        this.deleteMedication(this.state.selectedRow.id);

    }
    toggleFormUpdate() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }

    selectRow=(rowIndex) =>{
        this.setState({
            selectedRow: rowIndex
        });
        //console.log(rowIndex);
    }

    reload() {
        this.setState({
            isLoaded: false,
            selectedRow:null
        });
       this.fetchMedications();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Medication Management </strong>
                </CardHeader>
                <Card >
                    <br/>
                    <Row >
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormAdd}>Add Medication </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormDelete}>Delete Medication </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormUpdate}>Update Medication </Button>
                        </Col>

                    </Row>

                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData} onRowClick={this.selectRow}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

               <Modal isOpen={this.state.selectedAdd} toggle={this.toggleFormAdd}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormAdd}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationUpdateForm reloadHandler={this.reload} rowData={this.state.selectedRow}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default MedicationContainer;
