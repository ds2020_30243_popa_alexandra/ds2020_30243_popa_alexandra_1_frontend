import React from "react";
import Table from "../../commons/tables/table";
//import Checkbox from '@material-ui/core/Checkbox';

const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },

];

const filters = [
    {
        accessor: 'name',
    }
];

class CaregiverTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }
    handleSelect = function(state) {
        console.log(state);
        return state.isOpen;
    }

    onCaregiverRowClick=(rowInfo)=>{
        this.props.onRowClick(rowInfo)
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
                onRowClick={this.onCaregiverRowClick}

            />
        )
    }
}

export default CaregiverTable;