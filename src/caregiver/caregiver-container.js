import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import CaregiverForm from "./components/caregiver-form";
import CaregiverUpdateForm from "./components/caregiver-update-form";

import * as API_USERS from "./api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";

class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormAdd = this.toggleFormAdd.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedAdd: false,
            selectedDelete: false,
            selectedUpdate: false,
            selectedRow:null,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteCaregiver(caregiverId) {
        return API_USERS.deleteCaregiver(caregiverId, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted caregiver with id: " + result);
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    toggleFormAdd() {
        this.setState({selectedAdd: !this.state.selectedAdd});

    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
        console.log(this.state.selectedRow.id);
        this.deleteCaregiver(this.state.selectedRow.id);

    }
    toggleFormUpdate() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    selectRow=(rowIndex) =>{
        this.setState({
            selectedRow: rowIndex
        });
    }

    reload() {
        this.setState({
            isLoaded: false,
            selectedRow:null
        });
        this.fetchCaregivers();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Caregiver Management </strong>
                </CardHeader>
                <Card >
                    <br/>
                    <Row >
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormAdd}>Add Caregiver </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormDelete}>Delete Caregiver </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormUpdate}>Update Caregiver </Button>
                        </Col>

                    </Row>

                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData} onRowClick={this.selectRow}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedAdd} toggle={this.toggleFormAdd}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormAdd}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverUpdateForm reloadHandler={this.reload} rowData={this.state.selectedRow}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default CaregiverContainer;
