import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    NavbarToggler,
    UncontrolledDropdown, Col
} from 'reactstrap';


const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBarDoctor = () => (

    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/doctorOptions/patient">Patient</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctorOptions/caregiver">Caregiver</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctorOptions/medication">Medication</NavLink>
                        </DropdownItem>

                    </DropdownMenu>

                </UncontrolledDropdown>
            </Nav>
        </Navbar>
    </div>
);

export default NavigationBarDoctor
