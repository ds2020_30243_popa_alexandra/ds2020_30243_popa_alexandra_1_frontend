import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import MedicationContainer from "./medication/medication-container";
import LoginContainer from "./login/login-container";
import DoctorOptionsContainer from "./doctorOptions/doctorOptions-container";
import PatientOptionContainer from "./patientOption/patientOption-container";
import CaregiverOptionContainer from "./caregiverOption/caregiverOption-container";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.toggleLoginOn = this.toggleLoginOn.bind(this);

        this.state = {
            loginOn: false,
            userId:null
        };

    }
    toggleLoginOn(rez){
        this.setState({
                loginOn:rez
            }
        )
    }



    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    {!this.state.loginOn  && <NavigationBar />}

                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />
                        <Route
                            exact
                            path='/login'
                            render={() => <LoginContainer  toggleLoginOn={this.toggleLoginOn} />}
                        />
                        <Route
                            exact
                            path='/doctorOptions/patient'
                            render={() => <PatientContainer />}
                        />
                        <Route
                            exact
                            path='/doctorOptions/caregiver'
                            render={() => <CaregiverContainer/>}
                        />
                        <Route
                            exact
                            path='/doctorOptions/medication'
                            render={() => <MedicationContainer/>}
                        />
                        <Route
                            exact
                            path='/doctorOptions'
                            render={() => <DoctorOptionsContainer toggleLoginOn={this.toggleLoginOn}/>}
                        />
                        <Route
                            exact
                            path='/patientOption'
                            render={() => <PatientOptionContainer toggleLoginOn={this.toggleLoginOn}/>}
                        />
                        <Route
                            exact
                            path='/caregiverOption'
                            render={() => <CaregiverOptionContainer toggleLoginOn={this.toggleLoginOn}/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
