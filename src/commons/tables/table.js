import React, {Component} from "react";
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import Field from "./fields/Field";
import {Col, Row} from "react-bootstrap";


class Table extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data,
            columns: props.columns,
            search: props.search,
            filters: [],
            getTrPropsFunction: props.getTrProps,
            pageSize: props.pageSize || 10,
            selectedRow:null

        };
    }

    search() {

    }

    filter(data) {
        let accepted = true;

        this.state.filters.forEach(val => {
            if (String(val.value) === "") {
                accepted = true;
            }

            if (!String(data[val.accessor]).includes(String(val.value)) && !String(val.value).includes(String(data[val.accessor]))) {
                accepted = false;
            }
        });

        return accepted;
    }

    handleChange(value, index, header) {
        if (this.state.filters === undefined)
            this.setState({filters: []});

        this.state.filters[index] = {
            value: value.target.value,
            accessor: header
        };

        this.forceUpdate();
    }


    getTRPropsType=(state,rowInfo)=> {

        //this.state.onRowClick(state,rowInfo);
        if (rowInfo && rowInfo.row) {
            return {
                onClick: (e) => {
                    if(this.state.selectedRow==null ){
                        this.setState({
                            selectedRow: rowInfo.index
                        })}
                    else {
                        if(this.state.selectedRow!=rowInfo.index)
                        this.setState({
                            selectedRow: rowInfo.index
                        })
                        else
                            this.setState({
                                selectedRow: null
                            })
                        }
                    //console.log(this.state.data[rowInfo.index])
                    this.props.onRowClick(this.state.data[rowInfo.index]/*rowInfo.index*/)
                },
                style: {
                    background: rowInfo.index === this.state.selectedRow ? '#00afec' : 'white',
                    //color: rowInfo.index === this.state.selected ? 'white' : 'black'
                }
            }
        }else{
            return {}
        }
    }



    render() {
        let data = this.state.data ? this.state.data.filter(data => this.filter(data)) : [];

        return (
            <div>

                <Row>
                    {
                        this.state.search.map((header, index) => {
                            return (
                                <Col key={index}>
                                    <div >
                                        <Field id={header.accessor} label={header.accessor}
                                               onChange={(e) => this.handleChange(e, index, header.accessor)}/>
                                    </div>
                                </Col>
                            )
                        })
                    }
                </Row>
                <Row >
                    <Col>
                        <ReactTable
                            data={data}
                            resolveData={data => data.map(row => row)}
                            columns={this.state.columns}
                            defaultPageSize={this.state.pageSize}
                            getTrProps={this.getTRPropsType}
                            showPagination={true}
                            style={{
                                height: '300px'
                            }}
                            //getTrProps={(state,rowInfo)=>this.onRowClick(state, rowInfo)}



                        />
                    </Col>

                </Row>
            </div>
        )
    }
}

export default Table;
