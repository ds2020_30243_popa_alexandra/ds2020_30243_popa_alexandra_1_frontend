import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Card, Col, Container, Jumbotron, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';


import PatientContainer from "../patient/patient-container";
import CaregiverContainer from "../caregiver/caregiver-container";
import MedicationContainer from "../medication/medication-container";


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class DoctorOptionsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedLogin: false,
            isLoaded: false,
            errorStatus: 0,
            error: null,
            userId:this.props.userId
        };
    }

    logOut=()=>{
        sessionStorage.removeItem('userId');
        window.location.href = '/';
    }
    componentDidMount() {
        this.props.toggleLoginOn(true);
    }


    render() {

        return (

           <div>
               <PatientContainer userId={this.state.userId}/>
               <CaregiverContainer/>
               <MedicationContainer/>
               <Card >
                   <br/>
                   <Row >
                       <Col sm={{size: '8', offset: 1}}>
                           <Button color="primary" onClick={this.logOut}> LogOut </Button>
                       </Col>
                   </Row>
               </Card >
           </div>
        )
    };
}

export default DoctorOptionsContainer;
