import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient',
    patient_caregiver:'/patient_caregiver',
    medicationPlan:'/medicationPlan',
    medicationListItem:'/medicationListItem'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/'+ id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function deletePatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.patient +'/'+user, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function addCaregiver(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postMedicationPlan(medPlan, callback){
    console.log(medPlan)
    let request = new Request(HOST.backend_api + endpoint.medicationPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postMedicationListItem(medicationListItem, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationListItem , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationListItem)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function getMedicationDetails(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationListItem, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlanByPatientId(id,callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan+'/'+id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientsByCaregiverId(id,callback) {
    let request = new Request(HOST.backend_api + endpoint.patient_caregiver+'/'+id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    getPatientById,
    postPatient,
    putPatient,
    deletePatient,
    addCaregiver,
    postMedicationPlan,
    postMedicationListItem,
    getMedicationDetails,
    getMedicationPlanByPatientId,
    getPatientsByCaregiverId
};
