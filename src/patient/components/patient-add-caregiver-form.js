import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/patient-api";
import * as API_USERS_CAREGIVER from "../../caregiver/api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import CaregiverTable from "../../caregiver/components/caregiver-table"


class PatientAddCaregiverForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            selectedCaregiver:null,
            formIsValid: false,


        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    addCaregiver(patient) {
        return API_USERS.addCaregiver(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully add caregiver to patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    selectRow=(rowIndex) =>{
        this.setState({
            selectedCaregiver: rowIndex,
            formIsValid: true
        });
    }
    handleSubmit() {
        let patient = {
            id:this.props.rowData.id,
            name: this.props.rowData.name,
            birthDate: this.props.rowData.birthDate,
            gender: this.props.rowData.gender,
            address: this.props.rowData.address,
            medicalRecord:this.props.rowData.medicalRecord,
            caregiver:this.state.selectedCaregiver.name
        };


        this.addCaregiver(patient);

    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_USERS_CAREGIVER.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }
    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Choose a caregiver </strong>
                </CardHeader>
                <Card >
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData} onRowClick={this.selectRow}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }



            </div>
        ) ;
    }
}

export default PatientAddCaregiverForm;
