import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Medical Record',
        accessor: 'medicalRecord',
    },

];

const filters = [
    {
        accessor: 'name',
    }
];

class PatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }
    handleSelect = function(state) {
        console.log(state);
        return state.isOpen;
    }

    onPatientRowClick=(rowInfo)=>{
        this.props.onRowClick(rowInfo)
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
                onRowClick={this.onPatientRowClick}
            />
        )
    }
}

export default PatientTable;