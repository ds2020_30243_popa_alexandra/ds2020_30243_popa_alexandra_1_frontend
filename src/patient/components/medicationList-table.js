import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Medication Name',
        accessor: 'medicationName',
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },
    {
        Header: 'Intake Intervals',
        accessor: 'intakeIntervals',
    },


];

const filters = [
    {
        accessor: 'medicationName',
    }
];

class MedicationListTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    onPatientRowClick=(rowInfo)=>{
        this.props.onRowClick(rowInfo)
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
                onRowClick={this.onPatientRowClick}

            />
        )
    }
}

export default MedicationListTable;