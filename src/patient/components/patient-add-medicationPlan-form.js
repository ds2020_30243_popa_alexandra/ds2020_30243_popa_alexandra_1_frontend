import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/patient-api";
import * as API_USERS_MEDICATION from "../../medication/api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import MedicationTable from "../../medication/components/medication-table";
import MedicationListTable from "./medicationList-table";



class PatientAddMedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            selectedMedication:[],
            formIsValid: false,
            planId:null,
            medicationItem:null,
            medicationDetails:[],
            medicationTableView:[],
            formControls: {
                periodOfTheTreatment: {
                    value: '',
                    placeholder: 'YYYY-MM-DD - YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        //periodValidator: true,
                        isRequired: true
                    }
                },
                intakeIntervals: {
                    value: '',
                    placeholder: 'pills/day',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

            }

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;


        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    createMedicationPlan(medicationPlan) {
        return API_USERS.postMedicationPlan(medicationPlan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully create medicationPlan to patient with id: " + result);
                this.reloadHandler();
                this.setState(({
                    planId:result,

                }));
            } else {
                this.setState(({
                    //planId:result,
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    createMedicationListItem(medicationListItem) {
        return API_USERS.postMedicationListItem(medicationListItem, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully create postMedicationListItem to patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    getMedicationDetails() {
        return API_USERS.getMedicationDetails( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get medication details to patient with id: " + result);
                this.reloadHandler();

                this.setState(({
                    medicationDetails:result
                }));
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }

        });
    }
    selectRow=(rowData) =>{
        this.setState({
            selectedMedication: rowData,
            formIsValid: true
        });
    }
    handleSubmit() {

            let medicationPlan = {
                patientUser:this.props.selectedRowData.id,
                doctorUser:sessionStorage.getItem("userId"),
                periodOfTheTreatment: this.state.formControls.periodOfTheTreatment.value
            };


            this.createMedicationPlan(medicationPlan);


    }
    addMedicationToPlan=()=> {

        let medicationListItem = {
            intakeIntervals:this.state.formControls.intakeIntervals.value,
            medication: this.state.selectedMedication.id,
            medicationPlan: this.state.planId

        };
        let medicationListItemView = {

            medicationName: this.state.selectedMedication.name,
            dosage: this.state.selectedMedication.dosage,
            intakeIntervals: this.state.formControls.intakeIntervals.value

        };

        this.createMedicationListItem(medicationListItem);
        let newMedicationTableView=this.state.medicationTableView;
        newMedicationTableView.push(medicationListItemView);
        console.log("MedicationList: " +JSON.stringify(medicationListItemView));
        this.setState({
            medicationTableView:newMedicationTableView
        });
        console.log("MedicationList: " +JSON.stringify(newMedicationTableView));

    }



    componentDidMount() {
        this.fetchMedicationAndPlan();
    }

    fetchMedicationAndPlan() {

        return API_USERS_MEDICATION.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

    }
    render() {
        return (
            <div>

                <FormGroup id='periodOfTheTreatment'>
                    <Label for='periodOfTheTreatmentField'> Period Of The Treatment: </Label>
                    <Input name='periodOfTheTreatment' id='periodOfTheTreatmentField' placeholder={this.state.formControls.periodOfTheTreatment.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.periodOfTheTreatment.value}
                           touched={this.state.formControls.periodOfTheTreatment.touched? 1 : 0}
                           valid={this.state.formControls.periodOfTheTreatment.valid}
                           required
                    />
                    {this.state.formControls.periodOfTheTreatment.touched && !this.state.formControls.periodOfTheTreatment.valid &&
                    <div className={"error-message row"}> * Period Of The Treatment must have format : YYYY-MM-DD - YYYY-MM-DD </div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  onClick={this.handleSubmit}>  Create Plan </Button>
                    </Col>
                </Row>
                <Card >
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData} onRowClick={this.selectRow}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> Intake Interval: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField' placeholder={this.state.formControls.intakeIntervals.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeIntervals.value}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                           valid={this.state.formControls.intakeIntervals.valid}
                           required
                    />
                    {this.state.formControls.intakeIntervals.touched && !this.state.formControls.intakeIntervals.valid &&
                    <div className={"error-message"}> * Intake Interval must have a valid format</div>}
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.addMedicationToPlan}>  Submit </Button>
                    </Col>
                </Row>
                <Card >
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <MedicationListTable  tableData = {this.state.medicationTableView} />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }



            </div>
        ) ;
    }
}

export default PatientAddMedicationPlanForm;
