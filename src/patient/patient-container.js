import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "./components/patient-form";
import PatientAddCaregiverForm from "./components/patient-add-caregiver-form";
import PatientAddMedicationPlanForm from "./components/patient-add-medicationPlan-form";
import PatientUpdateForm from "./components/patient-update-form";

import * as API_USERS from "./api/patient-api"
import PatientTable from "./components/patient-table";




class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormAdd = this.toggleFormAdd.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.toggleFormAddCaregiver = this.toggleFormAddCaregiver.bind(this);
        this.toggleFormAddMedicationPlan = this.toggleFormAddMedicationPlan.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedAdd: false,
            selectedDelete: false,
            selectedUpdate: false,
            selectedAddCaregiver: false,
            selectedAddMedicationPlan: false,
            selectedRow:null,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        };

    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

    }

    deletePatient(patientId) {
        //console.log(this.state.tableData[this.state.selectedRow]);
        return API_USERS.deletePatient(patientId, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient with id: " + result);
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    toggleFormAdd() {
        this.setState({selectedAdd: !this.state.selectedAdd});

    }
    toggleFormDelete() {
        this.setState({selectedDelete: !this.state.selectedDelete});
        console.log(this.state.selectedRow.id);
        this.deletePatient(this.state.selectedRow.id);

    }
    toggleFormUpdate() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }
    toggleFormAddCaregiver() {
        this.setState({selectedAddCaregiver: !this.state.selectedAddCaregiver});

    }
    toggleFormAddMedicationPlan() {
        this.setState({selectedAddMedicationPlan: !this.state.selectedAddMedicationPlan});

    }


    selectRow=(rowIndex) =>{
        this.setState({
            selectedRow: rowIndex
        });

        sessionStorage.setItem("selectedRow", this.state.selectedRow);
    }

    reload() {
        this.setState({
            isLoaded: false,

        });


       this.fetchPatients();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card >
                    <br/>
                    <Row >
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormAdd}>Add Patient </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormDelete}>Delete Patient </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormUpdate}>Update Patient </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormAddCaregiver}>Add Caregiver </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormAddMedicationPlan}>Add MedicationPlan </Button>
                        </Col>

                    </Row>

                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1,display:'inline'}}>
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData} onRowClick={this.selectRow}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

               <Modal isOpen={this.state.selectedAdd} toggle={this.toggleFormAdd}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormAdd}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <PatientUpdateForm reloadHandler={this.reload} rowData={this.state.selectedRow}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedAddCaregiver} toggle={this.toggleFormAddCaregiver}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormAddCaregiver}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <PatientAddCaregiverForm reloadHandler={this.reload} rowData={this.state.selectedRow}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedAddMedicationPlan} toggle={this.toggleFormAddMedicationPlan}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormAddMedicationPlan}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <PatientAddMedicationPlanForm  reloadHandler={this.reload} selectedRowData={this.state.selectedRow}/>
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}


export default PatientContainer;
